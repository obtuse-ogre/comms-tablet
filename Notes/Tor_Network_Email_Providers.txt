Tor Network Email Providers

Below is a list of email providers with .onion addresses.
Onion addresses are only accessible within the Tor network.

I personally have experience with both Protonmail, Bitmessage and Mail2Tor.
I recommend both Protonmail and Mail2Tor. 
I use Protonmail and Mail2Tor for completely different uses and both fulfill their role well.
Protonmail is what I use for daily communications. 
Mail2Tor is solely for communications which are conducted completely within the Tor network.
I have used Bitmessage and was pleased with the service but have not used it in some time so I am not comfortable recommending it.

I have no experience with the other providers so you will have to do your own research.

As an extra-warning the service identified as (NSFW aka 'not safe for work') will expose you to a lot of pornography. You should be able to guess from the name what you will see if you visit that site.

As a final note this list was compiled by someone other than me in June 2020.
They listed the email providers in their order of trust.  
I do not know the person who compiled the list or their reasons for the ranking. I am simply passing along the information.
(Services with the 'clearweb' notation are accessible via a traditional web address as well i.e. https://protonmail.com. The clearweb addresses are not listed.)

    ProtonMail – protonirockerxow.onion, clearweb
    Torbox – torbox3uiot6wchz.onion
    Bitmessage – bitmailendavkbec.onion, clearweb
    Mail2Tor – mail2tor2zyjdctd.onion
    RiseUp – nzh3fv6jc6jskki3.onion, clearweb
    Cock.li (NSFW) – cockmailwwfvrtqj.onion, clearweb
    Lelantos – lelantoss7bcnwbv.onion paid accounts only
    Autistici – wi7qkxyrdpu5cmvr.onion, clearweb
    AnonInbox – ncikv3i4qfzwy2qy.onion paid accounts only
    VFEMail – 344c6kbnjnljjzlz.onion, clearweb

Any input on the listed providers, additional providers or updates to the list are appreciated.
Please send them to me at obtuse-ogre@protonmail.com.