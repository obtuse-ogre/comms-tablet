# Comms Tablet

A repository of applications for use on a secure communications tablet running Android.
(all apps end with the .apk extension)


*** APPLICATION REPOSITORIES

F-Droid
https://f-droid.org/en/
[Updated 03/10/21]


*** BROWSING

DuckDuckGo Browser:
https://github.com/duckduckgo/Android/releases
[Updated 03/10/21]

Brave Browser (not included here but dowload link is below):
https://github.com/brave/brave-browser/releases
* The files for this application are too numerous and too large to include in this repository.
* Ther are multiple files based on the architecture of your processor.
* You can download the latest .apk for the processor in your device from the repository listed above.
Website: https://brave.com/


*** EMAIL 

Tutanota:
https://github.com/tutao/tutanota/releases
Website: https://tutanota.com
[Updated 03/10/21]

ProtonMail:
https://protonapps.com
Website: https://protonmail.com
[Updated 03/10/21]


*** ENCRYPTION

SSE (Secret Space Encryptor for Android)
https://paranoiaworks.mobi/download/
* Direct Download (not the link to the Google Play Store)
Website: https://paranoiaworks.mobi/sse/
[Updated 03/10/21]


*** MESSAGING - OFFLINE

Briar:
https://briarproject.org/apk/briar.apk
Website: https://briarproject.org/
[Updated 03/10/21]

Detailed instructions for downloading and installing using 
the internet connection on your phone or tablet can be found at:
https://briarproject.org/installing-briar-via-direct-download/

An alternative method is to install Briar from F-Droid.
You can use this link from within F-Droid to access the .apk:
fdroidrepos://briarproject.org/fdroid/repo?fingerprint=1FB874BEE7276D28ECB2C9B06E8A122EC4BCB4008161436CE474C257CBF49BD6
You can find more information about installing Briar via F-Droid here:
https://briarproject.org/installing-briar-via-f-droid/


*** MESSAGING - ONLINE

Wire:
https://wire.com/en/download/
Requires Android 7.0 or newer
Website: https://wire.com/en/
[Updated 03/10/21]

Signal:
https://signal.org/android/apk/
Requires Android 4.4 or newer
Website: https://signal.org
How To: https://ssd.eff.org/en/module/how-use-signal-android
[Updated 03/10/21]


*** RADIO

AndFlmsg:
https://sourceforge.net/projects/fldigi/files/AndFlmsg/
* This application and supporting documents are in their own directory.
* The location above is for the apk download together with the supporting documents. 
[Updated 12/06/20]


*** TOR

Orbot (Proxy with Tor):
https://guardianproject.info/releases/
[Updated 12/06/20]

Tor Browser:
https://www.torproject.org/download/#android
Requires Android 5.0 and up
* The files for this application are held in a separate directory.
* Ther are multiple files based on the architecture of your processor.
* Choose the apk for the processor in your device.
Website: https:torproject.org
[Updated 03/10/21]

An alternative method for installing the Tor Browser is to add the Guardian Project's Repository 
to F-Droid and install the .apk from their reposititory using F-Droid.
The link to the Guardian Project's Repository is: https://guardianproject.info/fdroid/
More information can be found at https://support.torproject.org/tormobile/tormobile-7/


*** VPN

Mullvad
https://mullvad.net/en/download/android/
Website: https://mullvad.net/en/

Instructions can be found at: https://mullvad.net/en/help/install-mullvad-app-android/
for installing from the .apk and via F-Droid.

NordVPN:
https://nordvpn.com/download/android/
Website: https://nordvpn.com
[Updated 03/10/21]

*** 
Both Mullvad and NordVPN can be purchased with cryptocurrencies and pre-paid cards / gift cards.
An anonymous purchase of a VPN service requires more than the use of cryptocurrencies or pre-paid/gift cards.
A guide to assist you in purchasing a VPN service with more anonymously will be published later in the 'Notes' section.